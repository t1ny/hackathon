# Hackathon 2020
## JampL

* Verkehr & Stau Info

# Rollen

 * Networker - Marie

 * Game Master - David

 * Developer - David, Niklas, Fynn, Luca, Marie

 * Designer - Luca, Niklas

 * Team Manager - Marie

 * Game Master - David

 * Time Keeper - Fynn

 * Recorder - Luca

# Ziele

## Primär: Stau Info
 
  * Ziel 1
    1. Echtzeit PKW Verkehr API 
    2. RMV API  für Bus und Bahn Verkehr API
    3. API Abfragen
    4. Datenbank
    5. LogIn Datenbank

## Sekundär: Kalender, Chat, Dateiablage

  * Ziel 2
    Kalender
    - Stau daten in Klalender einfügen

  * Ziel 3
    Dateiablage
    - OneDrive einbauen

  * Ziel 4
    Chat 

# Datenbank und LogIn

* Datenbank Name: log
  Tabellen:
    1. t_user 
        - u_id -> Integer
        - u_name -> Sting
        - u_password -> String
        - u_county -> String
        - u_town -> String
        - u_street -> String
        - u_house -> Integer
        - u_busstop -> String
        - u_trainstation ->String
        - u_transport -> Integer
            0 - Car
            1 - Bus
            2 - Train
            3 - Bus and Train
    2. t_destination
        - d_id -> Integer
        - d_name -> String
        - d_county -> String
        - d_town -> Sting
        - d_street -> String
        - d_house -> Interger
        - d_trainstop -> String
        - d_busstop -> String
    

    